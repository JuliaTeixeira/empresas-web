﻿import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import { SharedModule } from '@shared/shared.module';
import { DetailComponent } from '@modules/detail/detail.component';
import { HomeComponent } from '@modules/home/home.component';
import { LoginComponent } from '@modules/login/login.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circle,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '28px',
      primaryColour: '#57bbbc',
      secondaryColour: '#ccc',
      tertiaryColour: '#ccc'
    }),
    SharedModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    DetailComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }