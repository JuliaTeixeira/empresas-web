import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailComponent } from '@modules/detail/detail.component';
import { HomeComponent } from '@modules/home/home.component';
import { LoginComponent } from '@modules/login/login.component';

import { AuthGuard } from './_helpers';
import { Enterprise } from './_models/enterprise';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'detail', component: DetailComponent, data: Enterprise, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
