﻿export class Portfolio {
  enterprises_number: number;
  enterprises: Array<any>;
}

export class Investor {
  id: number;
  investor_name: string;
  email: string;
  city: string;
  country: string;
  balance: number;
  photo?: string;
  portfolio: Portfolio;
  portfolio_value: number;
  first_access: boolean;
  super_angel: boolean;
}

export class User {
  investor: Investor;
  enterprise: null;
  success: boolean;
}

export class UserSession {
  accessToken: string;
  client: string;
  uid: string;
}