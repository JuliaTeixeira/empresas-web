﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User, UserSession } from '@app/_models';



@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  headers: HttpHeaders;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  saveSession(auth_response: UserSession) {
    localStorage.setItem('appEmpresasIoasys', JSON.stringify(auth_response));

  }

  getSessionData() {
    const userSession: UserSession = JSON.parse(
      localStorage.getItem('appEmpresasIoasys')
    ) as UserSession;
    return userSession;
  }

  isAuthenticated() {
    const appEmpresasIoasys: UserSession = JSON.parse(
      localStorage.getItem('appEmpresasIoasys')
    ) as UserSession;
    if (appEmpresasIoasys != null) {
      return true;
    }
    return false;
  }

  login(username: string, password: string) {
    const body = {
      email: username,
      password: password
    }
    return this.http.post<any>(`${environment.apiUrl}/users/auth/sign_in`, body, { observe: 'response' as 'body' }).pipe(map(user => {
      return user;
    }));
  }


  logout() {
    try {
      localStorage.removeItem('appEmpresasIoasys');
      this.router.navigate(['/login']);

    } catch (error) {
      console.error(error)
    }
  }
}