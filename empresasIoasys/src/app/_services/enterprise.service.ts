﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Enterprise } from '@app/_models/enterprise';
import { AuthenticationService } from './authentication.service';
import { UserSession } from '@app/_models';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class EnterpriseService {

  headers: HttpHeaders;
  sessionData: UserSession

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private router: Router,
  ) {

    this.sessionData = this.authenticationService.getSessionData()
    if (this.sessionData) {
      this.headers = new HttpHeaders()
        .set('access-token', this.authenticationService.getSessionData().accessToken)
        .set('uid', this.authenticationService.getSessionData().uid)
        .set('client', this.authenticationService.getSessionData().client)
    } else {
      this.router.navigate(['/login']);
    }

  }

  getAll() {
    return this.http.get<Enterprise[]>(`${environment.apiUrl}/enterprises`, { headers: this.headers });
  }

  search(data: string) {
    return this.http.get(`${environment.apiUrl}/enterprises?name=` + data, { headers: this.headers });
  }
}