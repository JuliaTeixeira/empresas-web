import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Enterprise } from '@app/_models/enterprise';
import { Location } from '@angular/common';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  empresa: Enterprise;

  constructor(
    private router: Router,
    private location: Location
  ) {

    this.empresa = this.router.getCurrentNavigation().extras.state as Enterprise;
  }

  ngOnInit(): void {
  }

  back() {
    this.location.back();
  }
}
