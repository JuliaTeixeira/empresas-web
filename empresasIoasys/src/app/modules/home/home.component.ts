﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { AuthenticationService, EnterpriseService } from '@app/_services';
import { Enterprise } from '@app/_models/enterprise';
import { Router } from '@angular/router';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  loading = false;
  lstEnterprise: Array<Enterprise>;
  noData = false;

  constructor(
    private enterpriseService: EnterpriseService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.loadData();
  }


  searchOrCleanField(data) {
    if (data) {
      this.loading = true;
      try {
        this.enterpriseService.search(data).subscribe((data: any) => {
          this.loading = false;
          this.lstEnterprise = data.enterprises
          if (this.lstEnterprise.length <= 0) {
            this.noData = true
          }
        })
      } catch (error) {
        this.loading = false;

        console.error(error)
      }
    } else {
      return
    }

  }

  loadData(data?) {
    this.loading = true;

    if (data) {
      this.loading = false;
      this.searchOrCleanField(data)
    } else {
      try {
        this.enterpriseService.getAll().pipe(first()).subscribe((data: any) => {
          this.loading = false;
          this.lstEnterprise = data.enterprises;
          if (this.lstEnterprise) {
            this.noData = false;
          }
        });

      } catch (error) {
        console.error(error)
      }

    }
  }

  goToDetail(item: Enterprise) {
    this.router.navigateByUrl('/detail', { state: item });
  }

  logout() {
    this.loading = true;
    try {
      this.authenticationService.logout();
      this.loading = false;
    } catch (error) {
      console.error(error);
    }
  }
}