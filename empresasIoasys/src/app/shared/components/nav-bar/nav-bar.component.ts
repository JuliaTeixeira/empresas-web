import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EnterpriseService } from '@app/_services';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, first, tap } from 'rxjs/operators';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  searchWord = '';
  @Output() isSearch: boolean = false
  @Output() searchcriteria = new EventEmitter<String>();
  @Output() newLst: EventEmitter<any> = new EventEmitter()

  lstEnterprise = [];
  searchTerm = new Subject<string>();
  searchEnterTerm = new Subject<string>();
  loading = false;

  constructor() { }

  ngOnInit(): void {
    this.searchTerm.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      tap((data) => {
        this.loadData();
      })
    ).subscribe();

  }

  showInput() {
    this.isSearch = true;
  }

  searchOrCleanField(isClean?: boolean) {
    if (isClean) {
      this.searchWord = '';
      this.loadData();
    }
    this.searchcriteria.emit(this.searchWord);
  }

  loadData() {
    this.newLst.emit(this.searchWord);
  }

}
